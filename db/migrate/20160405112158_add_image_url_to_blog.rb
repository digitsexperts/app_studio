class AddImageUrlToBlog < ActiveRecord::Migration
  def up
    add_column :blogs, :image_url, :string
  end

  def down
    remove_column :blogs, :image_url, :string
  end
end
